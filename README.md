# Cotação de Bitcoin
O programa tem como objetivo receber o input de duas datas e retornar um gráfico que apresente a evolução da cotação da criptomoeda Bitcoin durante esse período.

## Descrição
- Foi utilizada a linguagem Javascript, junto com HTML e CSS.
- Para obtenção dos dados, o programa utiliza a [API do Coinpaprika](https://api.coinpaprika.com/).
- Para criação do gráfico, o programa utiliza a biblioteca [Chartjs](https://www.chartjs.org/). 

## Uso
Para rodar o projeto, é necessário possuir alguma IDE, como Visual Studio Code. Após o download do projeto através do BitBucket, só é necessário inicializar a aplicação através do index.html.