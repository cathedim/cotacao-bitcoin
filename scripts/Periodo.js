class Periodo {
    constructor(dataInicial, dataFinal) {
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
    }

    get dataInicial() {
        return this.dataInicial;
    }
    
    get dataFinal() {
        return this.dataFinal;
    }
}
